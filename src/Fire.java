public class Fire extends PObject {
    // Number of fruit the fire has already hit
    private int hits;

    public int getHits() {
        return hits;
    }

    public void setHits(int hits) {
        this.hits = hits;
    }

    /**
     * Increases the number of hits of fire
     * Use when fire hits
     */
    public void hit(){
        this.hits++;
    }


    /**
     * Creates a new fire that has no mass (not affected by gravity)
     */
    public Fire() {
        super("assets/fire.png");
        this.setSize(this.getSize().getWidth() - 3, this.getSize().getHeight() - 3);
        this.setMass(0);
        this.setVelocityY(-10);
        this.setHits(0);
    }
}