public class Dragon extends PObject {

    /* C O N S T A N T S */
    // Offset to prevent dragon leaving the bounds of the window
    public static final double OFFSET_X = 34.0;
    // Maximum distance that dragon ignores and doesn't go for
    public static final int IGNORED_DISTANCE = 2;
    // Dragon's default moving speed
    public static final int SPEED = 4;

    // Directions that dragon can move
    public enum Direction{
        Right,
        Left
    };


    /** Properties of dragon */
    private int lives;
    private boolean isAlive;
    private int targetPosition;
    private double timeToActivate;
    private boolean isRunning;
    private int fireTime;
    private Direction direction;


    /* Getters and setters */
    public int getLives() {
        return lives;
    }

    public void setLives(int lives) {
        this.lives = lives;
    }

    public boolean isAlive(){
        return this.isAlive;
    }

    public boolean isAvailable(){
        return this.getTimeToActivate() == 0;
    }

    public double getTimeToActivate() {
        return timeToActivate;
    }

    public void setTimeToActivate(double timeToActivate) {
        this.timeToActivate = timeToActivate;
    }

    public void setTargetPosition(double pos) {
        this.targetPosition = (int)Helper.Clamp(pos, 0 + OFFSET_X,  Game.APPLICATION_WIDTH - OFFSET_X);
    }

    public int getFireTime() {
        return fireTime;
    }

    public void setFireTime(int fireTime) {
        this.fireTime = fireTime;
    }

    public int getTargetPosition(){ return this.targetPosition; }

    /** Constructor */
    public Dragon() {
        super("assets/dragon_right.png");
        this.direction = Direction.Left;
        this.lives = 3;
        this.isAlive = true;
        this.isRunning = false;
        this.setTimeToActivate(0);
        this.fireTime = 0;
        this.setVelocityX(SPEED);
    }


    /** Method to move dragon towards its target position */
    public void move() {

        double distance = this.targetPosition - OFFSET_X - this.getX();

        // Moving right
        if (distance > IGNORED_DISTANCE) {
            this.direction = Direction.Right;
            if ((distance > 4)) {
                if (Time.now() % 5 == 0) {
                    if (this.isRunning) {
                        if (!this.isPassive())
                            this.setImage("assets/dragon_right.png");
                        else
                            this.setImage("assets/dragon_right_frozen.png");
                        this.isRunning = false;
                    }
                    else {
                        if (!this.isPassive())
                            this.setImage("assets/dragon_run_right.png");
                        else
                            this.setImage("assets/dragon_run_right_frozen.png");
                        this.isRunning = true;
                    }
                }
            } else {
                if (!this.isPassive())
                    this.setImage("assets/dragon_right.png");
                else
                    this.setImage("assets/dragon_right_frozen.png");
                this.isRunning = false;
            }
            this.setVelocityX(SPEED);
            super.move();
        }

        // Moving left
        else if (distance < -IGNORED_DISTANCE) {
            this.direction = Direction.Left;
            if ((distance < -6)) {
                if (Time.now() % 5 == 0) {
                    if (this.isRunning) {
                        if (!this.isPassive())
                            this.setImage("assets/dragon_left.png");
                        else
                            this.setImage("assets/dragon_left_frozen.png");
                        this.isRunning = false;
                    }
                    else {
                        if (!this.isPassive())
                            this.setImage("assets/dragon_run_left.png");
                        else
                            this.setImage("assets/dragon_run_left_frozen.png");
                        this.isRunning = true;
                    }
                }
            } else {
                if (!this.isPassive())
                    this.setImage("assets/dragon_left.png");
                else
                    this.setImage("assets/dragon_left_frozen.png");
                this.isRunning = false;
            }
            this.setVelocityX(-SPEED);
            super.move();
        }
    }

    /**
     * Dragon loses 1 of its lives and becomes passive fo 2.5 seconds
     */
    public void loseLife(){
        this.lives--;
        this.setPassive(true);
        if (this.lives <= 0)
            this.isAlive = false;
        this.setTimeToActivate(Time.now() + Time.fromSeconds(2.5));
        if (this.direction.equals(Direction.Left)) {
            this.setVelocityX(20);
            this.setTargetPosition(this.getX() + OFFSET_X + 100);
        }
        else {
            this.setVelocityX(-20);
            this.setTargetPosition(this.getX() + OFFSET_X - 100);
        }
        this.move();
    }
}
