import acm.graphics.GImage;

import java.util.ArrayList;


/** Physical Object class that adds physical characteristics to a Graphical Image class */

public class PObject extends GImage {


    /** Fields */
    private float mass;
    private double velocityX;
    private double velocityY;
    private static double gravity = 9.8;
    private boolean isPassive;


    /** Getters and setters */
    public float getMass() {
        return mass;
    }

    public void setMass(float mass) {
        this.mass = mass;
    }

    public double getVelocityX() {
        return velocityX;
    }

    public void setVelocityX(double velocityX) {
        this.velocityX = velocityX;
    }

    public double getVelocityY() {
        return velocityY;
    }

    public void setVelocityY(double velocityY) {
        this.velocityY = velocityY;
    }

    public boolean isPassive() {
        return isPassive;
    }

    public void setPassive(boolean passive) {
        this.isPassive = passive;
        this.mass = 0;
    }



    /** Constructor */
    public PObject(String image){
        super(image);
        this.velocityX = 0;
        this.velocityY = 0;
        this.isPassive = false;
    }


    /**
     *  Moves the object for one frame according to its physical properties
     *  */
    public void move()
    {
            super.move(this.velocityX, this.velocityY + (this.gravity * this.mass / 2));
            this.velocityY += this.gravity * this.mass;

    }

    
    /** Moves the object and its attached objects for one frame according to its
    *  physical properties
    *
    *  Attached objects do not have physical properties and move together with
    *  the object they are attached to (this object)
     */
    public void move(ArrayList<PObject> attachedObjects){
        super.move(this.velocityX, this.velocityY + (this.gravity * this.mass / 2));
        for (PObject object : attachedObjects)
        {
            object.move(this.velocityX, this.velocityY + (this.gravity * this.mass / 2));
        }
        this.velocityY += this.gravity * this.mass;
    }


}
