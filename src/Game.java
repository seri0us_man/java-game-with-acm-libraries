import acm.graphics.*;
import acm.program.GraphicsProgram;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;


public class Game extends GraphicsProgram {

    /* C O N S T A N T S */
    /** Game settings */
    public static final int APPLICATION_WIDTH = 480;
    public static final int APPLICATION_HEIGHT = 600;
    public static final int FPS = 60;

    /** Offsets and limits */
    public static final int GROUND_LEVEL_LIMIT = 120;
    public static final int DRAGON_FROM_GROUND = 82;

    /* Fields */
    /** Game Objects */
    private Dragon dragon;
    private Ground ground;
    private List<Fruit> fruits;
    private List<Fire> fires;
    private boolean gameIsOver;

    /** UI Elements */
    private int score;
    private int hiScore;
    private GLabel scoreTable;
    private GLabel restart;
    private GLabel endScore;
    private GLabel hiScoreLabel;
    private GLabel endHiScore;
    private List<GOval> healthBar;


    /** Initialize the game objects and UI */
    public void init()
    {
        setTitle("Fire Dragon");
        getHiScore();
        this.gameIsOver = false;

        // Load background
        GImage background = new GImage("assets/bg_image.png");
        add(background);

        // Load ground
        ground = new Ground();
        add(ground,0,ground.getLevel());

        /* Load dragon on the left, coming to the center of game window
           Initialize the array for storing its fires */
        dragon = new Dragon();
        add(dragon, 0, ground.getLevel() - DRAGON_FROM_GROUND);
        dragon.setTargetPosition(APPLICATION_WIDTH / 2);
        while (Helper.onInterval(dragon.getX(),dragon.getTargetPosition() - 1, dragon.getTargetPosition() + 1))
            dragon.move();
        fires = new ArrayList<>();

        // Initialize the array for storing fruit
        fruits = new ArrayList<>();

        // Load score table
        scoreTable = new GLabel("Score: " + this.score);
        scoreTable.setFont(new Font("Comic Sans MS", Font.BOLD, 18));
        scoreTable.setColor(Color.WHITE);
        this.score = 0;
        add(scoreTable,15,29);

        // Load health bar
        healthBar = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            GOval oval = new GOval(13,13);
            oval.setFillColor(Color.white);
            oval.setColor(Color.white);
            oval.setFilled(true);
            add(oval,405 + i * 22,16);
            healthBar.add(oval);
        }

        // Resets the game time
        Time.reset();

        // Add event listeners to detect mouse actions and motions
        addMouseListeners();
    }

    /**
     *  Updates the game elements until Dragon is dead or ground reaches its height limit
     */
    public void run() {
        while (dragon.isAlive() && ground.getLevel() != GROUND_LEVEL_LIMIT) {
            updateUI();
            updateGroundLevel();
            updateDragon();
            updateFires();
            updateFruit();
            detectCollisions();
            Time.update();
            pause(1000 / FPS);
        }
        GImage gameOver = new GImage("assets/gameover.png");
        gameOver.sendToFront();
        add(gameOver);
        this.gameIsOver = true;

        if (this.score > this.hiScore)
            setHiScore(Integer.toString(this.score));

        System.out.println(this.hiScore);

        endScore = new GLabel(Integer.toString(this.score));
        endScore.setColor(Color.white);
        endScore.setFont(scoreTable.getFont());
        add(endScore,APPLICATION_WIDTH / 2 - Integer.toString(this.score).length() * 4, 335);

        hiScoreLabel = new GLabel("High Score:");
        hiScoreLabel.setColor(Color.ORANGE);
        hiScoreLabel.setFont(endScore.getFont());
        add(hiScoreLabel,APPLICATION_WIDTH / 2 - 44, 390);

        endHiScore = new GLabel(Integer.toString(this.hiScore));
        endHiScore.setColor(Color.white);
        endHiScore.setFont(scoreTable.getFont());
        add(endHiScore,APPLICATION_WIDTH / 2 - Integer.toString(this.hiScore).length() * 4.5, 430);

        restart = new GLabel("Play Again");
        restart.setColor(Color.white);
        restart.setFont(new Font("Comic Sans MS", Font.BOLD, 20));
        restart.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                restart();
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                restart.setColor(new Color(0,0,0, 150));
            }

            @Override
            public void mouseExited(MouseEvent e) {
                restart.setColor(Color.white);
            }
        });
        add(restart, APPLICATION_WIDTH / 2 - 43, 540);
    }

    public void restart(){

        this.init();
        this.run();

    }


    /* Mouse listeners */
    /**
     * Changes target position of Dragon to cursor position
     */
    public void mouseMoved(MouseEvent e) {
        if (dragon != null && dragon.getTimeToActivate() - Time.now() < Time.fromSeconds(1.5)) {
            dragon.setTargetPosition(e.getX());
        }
    }

    /**
     * Fires a fire on mouse click if Dragon is not recently hit
     * by an ice or Dragon not recently fired a fire
     */
    public void mouseClicked (MouseEvent e){
        if (dragon != null && !gameIsOver && dragon.getFireTime() <= Time.now())       //Check when it's allowed to fire
        {
            Fire fire = new Fire();
            fires.add(fire);
            add(fire,dragon.getX() + 15,dragon.getY() - 40);
            dragon.setFireTime(Time.now() + Time.fromSeconds(0.5));
        }
    }



    /* State update methods */

    /** Updates User Interface for any changes */
    private void updateUI(){
        scoreTable.setLabel("Score: " + this.score);
    }


    /**
     * Updates the properties and position of the Dragon
     *
     * If it is not available (recently hit by an ice), updates its time to become available
     * If it is available, changes its position by moving towards the target position
     */
    private void updateDragon(){
        if (Time.now() == dragon.getTimeToActivate()) {
            dragon.setPassive(false);
        }
        dragon.move();
    }


    /**
     * Updates the positions of every fire inside the bounds of the game window
     *
     * If a fire just got out of bounds, removes it from the game window and from
     * the list of active fire objects.
     */
    private void updateFires(){
        Iterator<Fire> iter = fires.iterator();

        while (iter.hasNext()){
            Fire fire = iter.next();

            fire.move();
            fire.setSize(fire.getSize().getWidth() + 0.2, fire.getSize().getHeight() + 0.2);  // Make fire larger
            if (fire.getY() < 0)    // Fire left the bounds of game window
            {
                remove(fire);
                fire = null;
                iter.remove();
            }
        }
    }


    /**
     * Checks if it is time for a fruit fall, then generates a new fruit
     * and assigns a random time from interval for the next fall time
     *
     * Updates the position of every fruit in the game
     * */
    private void updateFruit(){
        if (Fruit.getFallTime().isNow())
        {
            Fruit fruit = new Fruit();
            int dropLocation = ThreadLocalRandom.current().nextInt(20, APPLICATION_WIDTH - 20);
            add(fruit,dropLocation,5);
            add(fruit.getIce(), dropLocation-15, -8 );
            fruits.add(fruit);

            // Set time for the next fruit fall
            Fruit.setFallTime(new Time(Time.now()
                    + ThreadLocalRandom.current().nextInt(50 - (FPS * 30 / Time.now()), 120 - (FPS * 30 / Time.now()))));
        }

        Iterator<Fruit> iter = fruits.iterator();
        while (iter.hasNext())
        {
            Fruit fruit = iter.next();
            fruit.move();
            // Make fruit blink if it's going to disappear after 1.5 seconds
            if (fruit.isPassive() && (fruit.getDisappearTime().getValue() - Time.now() < Time.fromSeconds(1.5)))
                fruit.setBlinking(true);
            fruit.updateBlinks();

            if (fruit.getY() > APPLICATION_HEIGHT || fruit.getDisappearTime().isNow()) {
                if (fruit.isFrozen()) {
                    remove(fruit.getIce());
                    ground.setLevel(ground.getLevel() - 20);
                }
                remove(fruit);
                fruit = null;
                iter.remove();
            }
        }
    }

    /**
     * Increases the ground level if it hasn't reached its target height yet
     * Created for animation purposes
     * Moves the dragon with the ground together
     * */
    private void updateGroundLevel(){
        if (ground.getY() != ground.getLevel())
        {
            ground.move(0, - 1);
            dragon.move(0, -1);
            for (Fruit fruit: fruits)
            {
                if (fruit.isPassive())
                    fruit.move(0,-1);
            }
        }
    }


    /**
     * Method that is responsible for every collision happening in the game
     *
     * As every collision in the game involves a fruit, it iterates through
     * the list of all fruit and checks its collision of it with Fire, Dragon
     * and the Ground respectively
     * */
    private void detectCollisions(){
        Iterator<Fruit> iter = fruits.iterator();

        while (iter.hasNext())
        {
            Fruit fruit = iter.next();


            /* Fruit gets hit by a fire
            *
            *  Ignores if a fruit is not frozen
            *  Otherwise, if collision happens, removes the ice from both game
            *  window and fruit object, changes the score accordingly and
            *  updates the number of fruit hit by the fire
            */
            if (fruit.isFrozen()) {
                for (Fire fire : fires) {
                        if (Helper.onInterval(fruit.getIce().getX(), fire.getX() - 40, fire.getX() + 20) &&
                                Helper.onInterval(fruit.getIce().getY(), fire.getY() - 20, fire.getY() + 20)) {
                            remove(fruit.getIce());
                            fruit.Thaw();
                            this.score += 10 + (fire.getHits() * 100);  //Every additional fruit hit by fire gives +100 points
                            fire.hit();
                            break;
                        }
                }
            }

            /* Fruit touches the Dragon
             *
             * Checks for collision if dragon is available
             * In case of collision, if the fruit is frozen, dragon loses life and becomes unavailable for 1 second
             * If the fruit is not frozen, dragon grabs the fruit and gains 10 points
             */
            if (!dragon.isPassive())
            {
                if (Helper.onInterval(fruit.getX(),dragon.getX() - 50, dragon.getX() + 65) &&
                        Helper.onInterval(fruit.getY(), dragon.getY() - 30, dragon.getY() + 80))
                {
                    if (fruit.isFrozen()) {
                        dragon.loseLife();
                        healthBar.get(dragon.getLives()).setFilled(false);
                    }
                    else {
                        this.score += 10;
                        remove(fruit);
                        fruit = null;
                        iter.remove();
                    }
                }
            }


            /* Fruit hits the ground
             *
             *  Changes the velocity of fruit by decreasing its magnitude by 3
             *  and changing its direction upwards
             *  If the magnitude of velocity is too small (tends to zero), neglects
             *  it and sets the fruit stationary
             */
            if (fruit!= null && !fruit.isFrozen())
            {
                if (Helper.onInterval(fruit.getY(), ground.getY() - 20, ground.getY()))
                {

                    if (!fruit.isPassive())
                    {
                        fruit.move(0, ground.getY() - 20 - fruit.getY());
                        fruit.setVelocityY(Math.ceil(fruit.getVelocityY() / -3));
                        if (fruit.getVelocityY() == 0 && !fruit.isPassive()) {
                            fruit.setPassive(true);
                            fruit.setDisappearTime(new Time(Time.now() + Time.fromSeconds(3)));
                        }
                    }
                }
            }
        }
    }

    private void setHiScore(String _score){
        try {
            FileWriter fileWriter =
                    new FileWriter(new File("./src/saveGame"));

            BufferedWriter bufferedWriter =
                    new BufferedWriter(fileWriter);

            bufferedWriter.write(_score);

            bufferedWriter.close();
        }
        catch(IOException ex) {
            System.out.println("ErrorC");
        }

        this.hiScore = Integer.parseInt(_score);
    }

    private void getHiScore(){
        String _hiScore = "";
        int hiScore = 0;

        try {
            FileReader fileReader =
                    new FileReader(new File("./src/saveGame"));

            BufferedReader bufferedReader =
                    new BufferedReader(fileReader);

            _hiScore = bufferedReader.readLine();

            bufferedReader.close();

            try{
                hiScore = Integer.parseInt(_hiScore);
            }
            catch (Exception e){ }

        }
        catch(FileNotFoundException ex) {
            System.out.println("ErrorA");
        }
        catch(IOException ex) {
            System.out.println("ErrorB");
        }

        this.hiScore = hiScore;
    }

    /** Method that starts the game */
    public static void main(String[] args)
    {
        new Game().start();
    }
}
