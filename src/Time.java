public class Time {
    //Current game time
    private static int now = 0;
    //Specific time value of Time object
    private int value;

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public boolean isNow(){
        return this.getValue() == now;
    }


    /**
     * Creates a new Time object with specific value
     * @param value
     */
    public Time(int value){

        this.value = value;
    }

    /**
     * Returns the current game time
     * @return
     */
    public static int now(){
        return now;
    }


    /**
     * Converts seconds to game time
     * @param seconds
     * @return
     */
    public static int fromSeconds(double seconds) { return (int)seconds * Game.FPS; }


    /**
     * Updates game time
     */
    public static void update(){
        now++;
    }

    /**
     * Resets game time
     */
    public static void reset() { now = 0; }



}
