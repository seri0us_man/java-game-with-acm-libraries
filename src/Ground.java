public class Ground extends PObject {

    /** Height of the ground at the start of the game */
    public static final int GROUND_INITIAL_HEIGHT = Game.APPLICATION_HEIGHT - 50;
    private int level;

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    /** Creates a new ground with default height */
    public Ground(){
        super("assets/ground.png");
        this.level = GROUND_INITIAL_HEIGHT;
    }
}
