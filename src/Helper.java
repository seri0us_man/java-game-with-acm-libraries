public final class Helper {

    /** Clamps the value between left and right bounds */
    public static double Clamp(double k, double left, double right)
    {
        if (k < left)
            return left;
        if (k > right)
            return right;
        return k;
    }

    /** Checks if the value is on the interval bounded by left and right values */
    public static boolean onInterval(double k, double left, double right)
    {
        if (k >= left && k <= right)
            return true;
        return false;
    }
}
