import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.ThreadLocalRandom;

public class Fruit extends PObject {


    private enum Type{
        Orange,
        GreenApple,
        RedApple
    }

    private Type type;
    private Ice ice;
    private boolean isBlinking;
    private boolean isFaded;
    private Time disappearTime;
    private static Time fallTime = new Time(Time.fromSeconds(2));


    public Type getType() { return type; }

    public boolean isFrozen() { return this.ice != null; }

    public Ice getIce() { return ice; }

    public static Time getFallTime() { return fallTime; }

    public static void setFallTime(Time fallTime) { Fruit.fallTime = fallTime; }

    public Time getDisappearTime() {
        return disappearTime;
    }

    public void setDisappearTime(Time disappearTime) {
        this.disappearTime = disappearTime;
    }


    /**
     * Creates a new fruit of a random type
     * Sets it a random speed that depends on the time of the game
     */
    public Fruit(){
        super("assets/RedApple.png");
        Type[] types = Type.values();
        this.type = types[ThreadLocalRandom.current().nextInt(0,3)];

        this.setImage("assets/" + this.type.toString() + ".png");

        this.setMass(0);
        this.ice = new Ice();
        this.setVelocityY((2 + (double)Time.now() / (Game.FPS * 60.0))
                            * ThreadLocalRandom.current().nextDouble(0.5, 1.0));
        this.setDisappearTime(new Time(0));  // by default unreachable time to avoid null
        this.isBlinking = false;
        this.isFaded = false;
    }

    /**
     * Moves the fruit uniquely depending on its state
     */
    public void move(){
        if (this.isFrozen())
            super.move(new ArrayList<>(Arrays.asList(this.ice)));
        else
            super.move();
    }

    /**
     * Ice gets melt and fruit starts a free fall
     */
    public void Thaw(){
        this.setVelocityY(0);
        this.setMass(0.015f);   // starts getting affected byg gravity
        this.ice = null;
    }

    public boolean isBlinking() {
        return isBlinking;
    }

    public void setBlinking(boolean blinking) {
        this.isBlinking = blinking;
    }

    public boolean isFaded() {
        return isFaded;
    }

    public void setFaded(boolean faded) {
        this.isFaded = faded;
    }

    /**
     * Updates the animation of blinking
     */
    public void updateBlinks(){
        if (this.isBlinking() && Time.now() % 7 == 0) {
            if (this.isFaded()) {
                super.setImage("assets/" + this.type.toString() + ".png");
                this.isFaded = false;
            }
            else {
                super.setImage("assets/" + this.type.toString() + "_faded.png");
                this.isFaded = true;
            }
        }
    }

}
